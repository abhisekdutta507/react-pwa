const cacheApp = 'ReactPWA';

this.addEventListener('install', (event) => {
  console.log('@developers - install event', event);

  event.waitUntil(
    caches.open(cacheApp).then(async (cache) => {
      const manifest = await fetch('asset-manifest.json').then(r => r.json());
      console.log('@developers - manifest', manifest);

      cache.addAll([
        // google fonts to be cached
        'https://fonts.googleapis.com/css2?family=Montserrat:wght@200;400;600&display=swap',
        'https://fonts.gstatic.com/s/montserrat/v15/JTURjIg1_i6t8kCHKm45_bZF3gnD_g.woff2',
        'https://fonts.gstatic.com/s/montserrat/v15/JTURjIg1_i6t8kCHKm45_aZA3gnD_g.woff2',
        'https://fonts.gstatic.com/s/montserrat/v15/JTUSjIg1_i6t8kCHKm459Wlhyw.woff2',

        // apis must be cached
        'https://jsonplaceholder.typicode.com/users',

        // chunk js, css files to be cached. get the links by inspecting the HTML template
        ...manifest.entrypoints,

        // manifest
        '/manifest.json',

        // icons
        '/favicon.ico',
        '/logo192.png',
        '/logo512.png',

        // index must be cached
        '/index.html',

        // routes to be cached
        '/about',
        '/users',
        '/'
      ]);
    })
  );
});

this.addEventListener('fetch', (event) => {
  console.log('@developers - fetch event', event);
  /**
   * @description when offline get the contents from cache
   */
  if (!navigator.onLine) {
    event.respondWith(
      caches.match(event.request).then((response) => {
        if (response) {
          return response;
        }
      })
    );
  }
});
