import './Header.css';
import { Link, NavLink } from 'react-router-dom';

function Header() {
  return (
    <div className="Header">
      <NavLink exact className="nav nav-home" activeClassName="active" to={'/'}>Home</NavLink>
      <NavLink exact className="nav nav-users" activeClassName="active" to={'/users'}>Users</NavLink>
      <NavLink exact className="nav nav-about" activeClassName="active" to={'/about'}>About</NavLink>
    </div>
  );
}

export default Header;
