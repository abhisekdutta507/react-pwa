import './Users.css';
import React, { Component } from 'react';
import Api from '../services/Api';

class Users extends Component {
  state = {
    loading: true,
    users: []
  }

  async componentDidMount() {
    const api = new Api();
    const users = await api.getUsers();

    // update the response
    this.setState({
      users,
      loading: false
    })
  }

  listUsers(users) {
    return (
      <>
        {
          !users.length
            ? <div>No user found!</div>
            : (
              <ul className="user-list">
                {
                  users.map(({ name, email }, index) => (
                    <li key={index} className="user-list-item">{name}</li>
                  ))
                }
              </ul>
            )
        }
      </>
    );
  }

  render() {
    const { users, loading } = this.state;

    return (
        <div className="Users">
          {
            !!loading
              ? <div>Loading users...</div>
              : this.listUsers(users)
          }
        </div>
    );
  } 
}

export default Users;
