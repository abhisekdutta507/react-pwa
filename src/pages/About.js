import './About.css';

function About() {
  return (
    <div className="About">
      <div className="heading pb-10">PWA in React</div>
      <div className="sub-heading pb-10">Author - Abhisek Dutta</div>
      <div className="description pb-10">Created for learning purpose.</div>
    </div>
  );
}

export default About;
