class Api {
  baseUrl = 'https://jsonplaceholder.typicode.com';

  async getUsers() {
    let r;
    try {
      r = await fetch(`${this.baseUrl}/users`).then((r) => r.json());
      return r;
    } catch(e) {
      return [];
    }
  }
}

export default Api;