import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import React, { Component } from 'react';

import Home from './pages/Home';
import Users from './pages/Users';
import About from './pages/About';
import Header from './comonents/Header';

class App extends Component {
  state = {}

  render() {
    return (
      <BrowserRouter>
        <Header />
        <div className="App">
          <Route exact path="/" component={(props) => <Home {...props} />} />
          <Route exact path="/users" component={(props) => <Users {...props} />} />
          <Route exact path="/about" component={(props) => <About {...props} />} />
        </div>
      </BrowserRouter>
    );
  } 
}

export default App;
